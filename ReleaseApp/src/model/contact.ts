export class Contact {
    private firstName : string;
    private lastName : string;
    private role : string;
    private number : string;
    private timezone : string;

    public Contact() : void {

    }

    public createUser(firstname, lastname, role, number, timezone) : void {
        this.firstName = firstname;
        this.lastName = lastname;
        this.role = role;
        this.number = number;
        this.timezone = timezone;
   }
}