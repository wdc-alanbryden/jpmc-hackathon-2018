import { INVALID } from "@angular/forms/src/model";

export class Release {
    private id: string;
    public title: string;
    private owners: string[];
    private start: Date;
    private end: Date;

    public newRelease(id, title, owners, start, end) {
        this.id = id;
        this.title = title;
        this.owners = owners;
        this.start = start;
        this.end = end;
    }

    public getId() : string {
        return this.id;
    }


}