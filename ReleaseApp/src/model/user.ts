export class User {
    private username : string;
    private role : string;
    private createDate: Date;
    private accountStatus: string;
    private contact : string;

    public User() : void {

    }

    public createUser(username, role, accountStatus, contact) : void {
        this.username = username;
        this.role = role;
        this.createDate = new Date();
        this.accountStatus = accountStatus;
        this.contact = contact;
    }
}