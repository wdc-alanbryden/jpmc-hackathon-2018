import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Contact } from '../../model/contact';

/**
 * Generated class for the ViewContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-contact',
  templateUrl: 'view-contact.html',
})
export class ViewContactPage {

  person : string;
  contact : Contact = new Contact();

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.person = navParams.get("person");
    this.contact.createUser('Jane', 'Doe', 'Developer','0743352617', 'EMEA');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewContactPage');
  }

}
