import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateReleasePage } from './create-release';

@NgModule({
  declarations: [
    CreateReleasePage,
  ],
  imports: [
    IonicPageModule.forChild(CreateReleasePage),
  ],
})
export class CreateReleasePageModule {}
