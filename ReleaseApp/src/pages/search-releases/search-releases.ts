import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Release } from '../../model/release';
import { ReleaseService } from '../../services/ReleaseService';

/**
 * Generated class for the SearchReleasesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-releases',
  templateUrl: 'search-releases.html',
})
export class SearchReleasesPage {
  searchQuery: string = '';
  releases: Release[];

  action: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private releaseService: ReleaseService) {
    this.initializeReleases();
    this.action = navParams.get('action');
  }

  private initializeReleases(): void{
    this.releases = this.releaseService.getReleases();
  }

  private getReleases(ev: any){
    this.initializeReleases();

    const val = ev.target.value;

    if (val && val.trim != '') {
      this.releases = this.releases.filter((release) => {
        return (release.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  private performAction(release) : void {
      switch(this.action) {
        case 'addContactToRelease' :
          this.addContactToRelease(release, this.navParams.get('contact'));
      }
  }

  private addContactToRelease(release, contact) : void {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchContactsPage');
  }
}
