import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchReleasesPage } from './search-releases';

@NgModule({
  declarations: [
    SearchReleasesPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchReleasesPage),
  ],
})
export class SearchReleasesPageModule {}
