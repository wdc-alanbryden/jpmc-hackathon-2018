import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CreateReleasePage } from '../create-release/create-release';
import { SearchContactsPage } from '../search-contacts/search-contacts';
import { SearchReleasesPage } from '../search-releases/search-releases';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  quickActions : any[];

  moreActions : any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeButtons();
  }

  private initializeButtons(){
    this.quickActions = [
      {
        name: 'Create Release',
        icon: 'create',
        page: CreateReleasePage,
        action: 'none'
      },
      {
        name: 'Add Contact to Release',
        icon: 'add',
        page: SearchContactsPage,
        action: 'addContactToRelease'
      },
      {
        name: 'Remove Contact from Release',
        icon: 'remove',
        page: SearchContactsPage,
        action: 'remove'
      },
      {
        name: 'Delete Release',
        icon: 'trash',
        page: SearchReleasesPage,
        action: 'deleteRelease'
      },
      {
        name: 'Close Release',
        icon: 'close',
        page: SearchReleasesPage,
        action: 'closeRelease'
      }
    ]

    this.moreActions = [
      {
        name: 'Settings',
        icon: 'cog',
        //settings page
      },
      {
        name: 'Log Out',
        icon: 'log-out',
        //settings page
      },
      {
        name: 'Help',
        icon: 'help'
        //release selector
      }
    ]
  }

  openNavDetailsPage(item) {
    this.navCtrl.push(item.page, {
      action: item.action
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

}
