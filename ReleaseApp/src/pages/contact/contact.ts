import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AddContactPage } from '../add-contact/add-contact';
import { SearchContactsPage } from '../search-contacts/search-contacts';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController) {

  }

  private addContact() : void {
    this.navCtrl.push(AddContactPage);
  }

  private searchContacts(): void {
    this.navCtrl.push(SearchContactsPage);
  }

}
