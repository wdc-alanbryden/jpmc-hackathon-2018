import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the UserLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-login',
  templateUrl: 'user-login.html',
})
export class UserLoginPage {

  private username;
  private password;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.username = 'TestUser'
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserLoginPage');
  }

  private login() : void {
    this.navCtrl.setRoot(TabsPage, {
      user: this.username,
      password: this.password
    });
  }

}
