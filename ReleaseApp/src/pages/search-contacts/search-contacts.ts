import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddContactPage } from '../add-contact/add-contact';
import { SearchReleasesPage } from '../search-releases/search-releases';
import { ViewContactPage } from '../view-contact/view-contact';

/**
 * Generated class for the SearchContactsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-contacts',
  templateUrl: 'search-contacts.html',
})
export class SearchContactsPage {

  searchQuery: string = '';
  contacts: string[];
  action: string = 'none';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.initializeContacts();
    if (navParams.get('action') != null){
      this.action = navParams.get('action');
    }
  }

  private initializeContacts(): void{
    this.contacts = [
      'Joe Bloggs',
      'Jane Doe',
      'Adam Bland',
      'Alan Sharp'
    ];
  }

  private getContacts(ev: any){
    this.initializeContacts();

    const val = ev.target.value;

    if (val && val.trim != '') {
      this.contacts = this.contacts.filter((contact) => {
        return (contact.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  private performAction(contact) : void {
      switch(this.action) {
        case 'addContactToRelease' :
          this.navCtrl.push(SearchReleasesPage, {
            action: this.action,
            person: contact
          })
          break;
        case 'none' : this.navCtrl.push(ViewContactPage, {
          person: contact
        })
        break;
      }
  }

  private navigateToAddContacts(): void {
    this.navCtrl.push(AddContactPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchContactsPage');
  }

}
