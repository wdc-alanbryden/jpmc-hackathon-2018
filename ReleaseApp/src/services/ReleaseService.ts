import { Release } from "../model/release";

export class ReleaseService {

    releases: Release[] = [];

    constructor() {
        this.initializeReleases();
    }

    private initializeReleases() {
        for(let i=0; i< 10; i++){
            
            var release : Release = new Release();
            release.newRelease(i, 'HackathonTest' + i, 'Alan,Will,Amirta', new Date(), new Date());
            this.releases.push(release);
        }
    }

    private addRelease(release : Release) : void {
        this.releases.push(release);
    }

    private deleteRelease(releaseId : string) : void {
        for(var i=0; i< this.releases.length; i++) {
            if(this.releases[i].getId() == releaseId) {
                this.releases.splice(i,1);
            }
        }
    }

    public getReleases() : Release[] {
        return this.releases;
    }
}