import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UserLoginPage } from '../pages/user-login/user-login';
import { AddContactPage } from '../pages/add-contact/add-contact';
import { SearchContactsPage } from '../pages/search-contacts/search-contacts';
import { CreateReleasePage } from '../pages/create-release/create-release';
import { SearchReleasesPage } from '../pages/search-releases/search-releases';
import { ReleaseService } from '../services/ReleaseService';
import { ViewContactPage } from '../pages/view-contact/view-contact';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    UserLoginPage,
    AddContactPage,
    SearchContactsPage, 
    CreateReleasePage,
    SearchReleasesPage,
    ViewContactPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    UserLoginPage,
    AddContactPage,
    SearchContactsPage,
    CreateReleasePage,
    SearchReleasesPage,
    ViewContactPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ReleaseService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
